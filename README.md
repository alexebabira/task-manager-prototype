# Task Form Manager Prototype
This prototype is thought through as an alternative more versatile way of modelling different task specialisations needed for the Boostr Project. 
Instead of having each task type as a separate entity, there is only one central entity, a Form. The Form can have any number of fields of different types. However as opposed to more conventional data models of this kind, the data gathered while a form is filled by the user is not stored in a specific predefined entity but a FormData object tied directly to the form via many to one relationship.

## Getting started
Clone this repository somewhere on your local machine.

### Prerequisites
This app assumes that you have [nodejs](https://nodejs.org/en/download/), [angular-cli](https://cli.angular.io/) and [visual code](https://code.visualstudio.com/download) installed and ready to go.

### Installing and Running
- Go to the directory where the project is located and run npm install
- In visual code, go to Debug -> Open Configurations and make sure that you have this line 
`"program": "${workspaceRoot}\\src\\server\\index.js",`
- Then hit Debug -> Start Debugging and go to localhost:3000

## Usage
Select the desired fields from the field definition grid then click 'Add to Grid'. You should see another grid forming below with the fields selected being rendered as columns for different task forms/types. You can also manipulate the amount of data gotten from the remote servers by interacting with the sliders above the grids. More details in the app... 



