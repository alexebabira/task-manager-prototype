import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MetaTypesService {
  public dataTypes = [
    {
      id: 'object',
      Name: 'Object'
    },
    {
      id: 'number',
      Name: 'Number'
    },
    {
      id: 'date',
      Name: 'Date'
    },
    {
      id: 'boolean',
      Name: 'Boolean'
    },
    {
      id: 'datetime',
      Name: 'Datetime'
    },
    {
      id: 'string',
      Name: 'String'
    }
  ];

  public controlTypes = [
    {
      id: 'dxDropDownBox',
      Name: 'Drop Down',
    },
    {
      id: 'dxTagBox',
      Name: 'Reference Picker'
    },
    {
      id: 'dxTextBox',
      Name: 'Text Box'
    },
    {
      id: 'dxNumberBox',
      Name: 'Number Box'
    },
    {
      id: 'dxDateBox',
      Name: 'Date Picker'
    },
    {
      id: 'dxCheckBox',
      Name: 'Check Box'
    }
  ];

  constructor() {}
}
