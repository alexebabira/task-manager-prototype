import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  constructor() {}

  getDiffInSecondsFrom(date: Date) {
    return ((new Date().getTime() - date.getTime()) / 1000).toFixed(2);
  }
}
