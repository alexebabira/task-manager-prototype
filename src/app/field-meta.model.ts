export class FieldDefinition {
  Type: string;
  id: string;
  formSnapshot: { id: string; Name: string };
  constructor(
    public Name: string,
    public Label: string,
    public ControlType: string,
    public DataType: string
  ) {
    this.Type = 'FieldDefinition';
  }
}
