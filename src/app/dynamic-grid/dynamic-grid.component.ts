import { UtilService } from '../common/util.service';
import { FormData } from '../form-data.model';
import { FieldDefinition } from '../field-meta.model';
import { ToastrService } from 'ngx-toastr';
import { DbQueryService } from '../db-query.service';
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  AfterViewInit
} from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { BaseGridComponent } from '../base-grid.component';

@Component({
  selector: 'app-dynamic-grid',
  templateUrl: './dynamic-grid.component.html',
  styleUrls: ['./dynamic-grid.component.scss']
})
// TODO: merge code common for components that use the dxGrid
export class DynamicGridComponent extends BaseGridComponent
  implements OnInit, OnChanges, AfterViewInit {
  @Input()
  selectedColumns: FieldDefinition[];
  gridData: any[];

  constructor(
    dbQueryService: DbQueryService,
    toastr: ToastrService,
    utilService: UtilService
  ) {
    super(dbQueryService, toastr, utilService, 'TaskFormData');
  }

  ngOnInit() {
    this.remoteDataLoaded.subscribe(() => {
      if (this.selectedColumns) {
        this._compileDataSource();
      }
    });
  }
  ngAfterViewInit() {
    this.fetchDataset(300);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedColumns'] && this.selectedColumns && this.remoteData) {
      this._compileDataSource();
    }
  }

  getDataBySlider() {
    super.getDataBySlider();
  }

  private _compileDataSource() {
    const rawFormData = this.remoteData as FormData[];

    this.gridData = rawFormData.map(formDataItem => {
      const result = { Form: formDataItem.TaskFormDefinitionSnapshot.Name };
      formDataItem.FieldData.forEach(fieldData => {
        const matchedField = this.selectedColumns.filter(
          col => col.id === fieldData.FieldDefinitionId
        )[0];

        const key = matchedField
          ? matchedField.Name
          : fieldData.FieldDefinitionId;

        if (fieldData.Value && typeof fieldData.Value === 'object') {
          result[key] = fieldData.Value.Name;
        } else {
          result[key] = fieldData.Value;
        }
      });
      return result;
    });
  }
}
