import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DbQueryService } from './db-query.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FieldMetaComponent } from './field-meta/field-meta.component';
import { FormsModule } from '@angular/forms';
import {
  DxButtonModule,
  DxDataGridModule,
  DxSliderModule
} from 'devextreme-angular';

import {
  JsonSchemaFormModule,
  Bootstrap3FrameworkModule
} from 'angular2-json-schema-form';

import { ToastrModule } from 'ngx-toastr';
import { DynamicGridComponent } from './dynamic-grid/dynamic-grid.component';
import { FooterComponent } from './footer/footer.component';
import { FormBuilderComponent } from './form-builder/form-builder.component';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
  { path: 'form-builder', component: FormBuilderComponent },
  { path: 'home', component: HomeComponent },

  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    FieldMetaComponent,
    DynamicGridComponent,
    FooterComponent,
    FormBuilderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    DxButtonModule,
    DxDataGridModule,
    DxSliderModule,
    BrowserAnimationsModule,
    Bootstrap3FrameworkModule,
    JsonSchemaFormModule.forRoot(Bootstrap3FrameworkModule),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),

    ToastrModule.forRoot({
      timeOut: 5000
    })
  ],
  providers: [DbQueryService],
  bootstrap: [AppComponent]
})
export class AppModule {}
