import { DxDataGridComponent } from 'devextreme-angular';
import { UtilService } from './common/util.service';
import { ToastrService } from 'ngx-toastr';
import { DbQueryService } from './db-query.service';
import { ViewChild, EventEmitter } from '../../node_modules/@angular/core';

export abstract class BaseGridComponent {
  @ViewChild('grid')
  gridObj: DxDataGridComponent;

  remoteData: any[];
  sliderValue: number;

  remoteDataLoaded = new EventEmitter<void>();

  constructor(
    protected dbQueryService: DbQueryService,
    protected toastr: ToastrService,
    protected utilService: UtilService,
    protected gridDataType: string
  ) {}

  getDataBySlider() {
    if (!this.sliderValue || this.sliderValue === 0) {
      this.fetchDataset();
    } else {
      this.fetchDataset(this.sliderValue);
    }
  }

  fetchDataset(count?: number) {
    const initalTime = new Date();
    this.gridObj.instance.beginCustomLoading('Loading Data...');
    this.dbQueryService.get(this.gridDataType, count).subscribe(formData => {
      this.toastr.info(
        `It took ${this.utilService.getDiffInSecondsFrom(
          initalTime
        )} sec to get ${formData.length} ${
          this.gridDataType
        }(s) from the remote database`
      );
      this.remoteData = formData;
      this.remoteDataLoaded.emit();
      this.gridObj.instance.endCustomLoading();
    });
    // TODO: split this method to make it more readable
  }
}
