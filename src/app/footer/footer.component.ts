import { DbQueryService } from '../db-query.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor(
    private dbQueryService: DbQueryService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {}
  /**
   * This function was constructed just to fix some data integrity issues
   * Note: It's not possible to run update/delete/add operations directly through
   * azure portal.
   */
  fixFormDefInDb() {
    this.dbQueryService.get('TaskFormData').subscribe(taskFormData => {
      this.dbQueryService
        .get('TaskFormDefinition')
        .subscribe(taskFormDefinitions => {
          this.toastr.success('Data for fix fetched');

          taskFormData = taskFormData.filter(
            taskFormDataItem => taskFormDataItem['TaskFormDefinitionId']
          );

          this.toastr.info(`${taskFormData.length} records to be updated`);

          const updatedRecords = taskFormData.map(taskFormDataItem => {
            const formDef = taskFormDefinitions.filter(formDefinition => {
              return (
                formDefinition['id'] ===
                taskFormDataItem['TaskFormDefinitionId']
              );
            })[0];

            if (formDef) {
              // console.log('Form data updated');
              taskFormDataItem['TaskFormDefinitionSnapshot'] = {
                id: formDef['id'],
                Name: formDef['Name']
              };
              delete taskFormDataItem['TaskFormDefinitionId'];
            }
            return taskFormDataItem;
          });

          let index = 0;
          updatedRecords.forEach(document =>
            this.dbQueryService.update(document).subscribe(result => {
              this.toastr.info(`Remote ${++index} document updated`);
            })
          );
        });
    });
  }
}
