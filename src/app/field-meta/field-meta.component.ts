import { FieldDefinition } from './../field-meta.model';
import { DynamicGridComponent } from '../dynamic-grid/dynamic-grid.component';
import { UtilService } from '../common/util.service';
import { MetaTypesService } from '../meta-types.service';
import { DbQueryService } from '../db-query.service';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  Input
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DxDataGridComponent } from 'devextreme-angular';
import { BaseGridComponent } from '../base-grid.component';

@Component({
  selector: 'app-field-meta',
  templateUrl: './field-meta.component.html',
  styleUrls: ['./field-meta.component.scss']
})
export class FieldMetaComponent extends BaseGridComponent
  implements OnInit, AfterViewInit {
  selectedRows: FieldDefinition[] = [];

  @Input()
  group = false;

  @Input()
  pageSize = 10;

  @Input()
  type = 'full';

  @Output()
  rowsSelected = new EventEmitter<FieldDefinition[]>();

  editFormConfig = {
    items: [
      {
        itemType: 'group',
        caption: 'Details',
        items: [ 'Name', 'Label', 'DataType', 'ControlType']
      },
      // {
      //   itemType: 'group',
      //   caption: '',
      //   items: [ 'Name', 'Label', 'DataType', 'ControlType']
      // }
    ]
  };

  constructor(
    public metaTypesService: MetaTypesService,
    dbQueryService: DbQueryService,
    utilService: UtilService,
    toastr: ToastrService
  ) {
    super(dbQueryService, toastr, utilService, 'FieldDefinition');
  }

  onContentReady(e) {
    e.component.columnOption('command:edit', {
      visibleIndex: -1
    });
  }
  ngOnInit() {}

  ngAfterViewInit() {
    this.fetchDataset();
  }

  getDataBySlider() {
    if (this.type !== 'basic') {
      super.getDataBySlider();
    }
  }

  deleteFieldDefinition($event: any) {
    const fieldDefinition = $event.data;
    this.dbQueryService.delete(fieldDefinition).subscribe(res => {
      this.toastr.success(
        `Field ${
          fieldDefinition.Name
        } has been deleted from the remote database`
      );
    });
  }

  addFieldDefinition($event: any) {
    const data = $event.data;

    if (!data.Name || !data.Label || !data.ControlType || !data.DataType) {
      $event.cancel = true;
      this.toastr.error(`Please fill in all the inputs`);
    } else {
      const add = new FieldDefinition(
        data.Name,
        data.Label,
        data.ControlType,
        data.DataType
      );
      // TODO:rollback if the addition failed
      this.dbQueryService.add(add).subscribe(fieldDefinition => {
        this.toastr.success(
          `Remote database has been updated with ${
            (fieldDefinition as FieldDefinition).Name
          }`
        );
      });
    }
  }

  updateFieldDefinition($event: any) {
    // replace the new values;
    const fieldDefinition = $event.key as FieldDefinition;
    const newData = $event.newData;

    Object.assign(fieldDefinition, newData);
    this.dbQueryService.update(fieldDefinition).subscribe(fd => {
      this.toastr.success(
        `Remote database has been updated for ${fieldDefinition.Name}`
      );
    });
  }

  onToolbarPreparing($event) {
    $event.toolbarOptions.items.unshift({
      location: 'before',
      widget: 'dxButton',
      options: {
        width: 136,
        text: 'Add to Grid',
        onClick: () => {
          this.rowsSelected.emit(this.selectedRows);
        }
      }
    });
  }
}
