import { FieldDefinition } from './../field-meta.model';
import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss']
})
export class FormBuilderComponent implements OnInit {
  formSchema: any;
  formLayout: any;

  newFieldsSelected($event) {
    const fromProp = this.compileForm($event);
    this.formSchema = fromProp.schema;
    this.formLayout = fromProp.layout;
  }

  compileForm(fields: FieldDefinition[]) {
    const schema = {
      type: 'object',
      properties: {}
    };

    const layout = [];

    // { "key": "birthday", "type": "date" },

    fields.forEach(field => {
      let jsonFromDataType;
      // If the field is of type date or datetime the control will be date
      // if the field is of type object the control will be a select box
      // otherwise infer the control from the datatype, text/number
      if (field.DataType === 'date' || field.DataType === 'datetime') {
        jsonFromDataType = 'string';
        layout.push({ key: field.Name, type: 'date' });
      } else if (field.DataType === 'object') {
        layout.push({
          key: field.Name,
          type: 'select'
        });

        jsonFromDataType = field.DataType;
      } else {
        layout.push({
          key: field.Name
        });
        jsonFromDataType = field.DataType;
      }

      schema.properties[field.Name] = {
        type: jsonFromDataType
      };
    });

    return { schema: schema, layout: layout };
  }

  constructor() {}

  ngOnInit() {}
}
