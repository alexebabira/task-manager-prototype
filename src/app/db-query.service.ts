import { FieldDefinition } from './field-meta.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
const api = '/api';

@Injectable({
  providedIn: 'root'
})
export class DbQueryService {
  constructor(private http: HttpClient) {}

  get(type: string, count?: number) {
    let params;
    if (count) {
      params = new HttpParams().set('count', '' + count);
    }
    return this.http.get<Array<Object>>(`${api}/queryCollection/${type}`, {
      params: params
    });
  }

  delete(document: Object) {
    return this.http.delete(`${api}/deleteDocument/${document['id']}`);
  }

  add(document: Object) {
    return this.http.post<Object>(`${api}/createDocument`, document);
  }

  update(document: Object) {
    return this.http.put<Object>(
      `${api}/updateDocument/${document['id']}`,
      document
    );
  }
}
