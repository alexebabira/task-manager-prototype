import { FieldDefinition } from './field-meta.model';
export class FormData {
  FieldData: { FieldDefinitionId: string; Value: any }[];
  Type: string;
  id: string;

  constructor(
    public TaskId: string,
    public TaskFormDefinitionSnapshot: { id: string; Name: string }
  ) {
    this.Type = 'TaskFormData';
  }
}
