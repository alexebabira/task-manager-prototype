const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const routes = require('./routes');

const root = 'dist/task-manager-prototype';
const port = process.env.Port || 3000;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(express.static(root));
app.use('/api', routes);

app.get('*', (req, res) => {
    res.sendFile(`index.html`, { root });
})

app.listen(port, () => {
    console.log(`Api running at localhost ${port}`);
})
