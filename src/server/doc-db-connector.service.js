// ADD THIS PART TO YOUR CODE
'use strict';

var documentClient = require('documentdb').DocumentClient;
const uriFactory = require('documentdb').UriFactory;
var config = require('./environment');

// ADD THIS PART TO YOUR CODE
var client = new documentClient(config.endpoint, {
  masterKey: config.primaryKey
});

var databaseId = config.database.id;
var collectionId = config.collection.id;

function checkError(res, err, result) {
  if (err) {
    res.status(500).send(err);
  } else {
    res.status(200).json(result);
  }
}

function createDocument(req, res) {
  let collectionUrl = uriFactory.createDocumentCollectionUri(
    databaseId,
    collectionId
  );
  client.createDocument(collectionUrl, req.body, (error, created) => {
    checkError(res, error, created);
  });
}

// ADD THIS PART TO YOUR CODE
function queryCollection(req, res) {
  var count = req.query.count;

  var countConstraint = count ? `TOP ${count}` : '';

  let collectionUrl = uriFactory.createDocumentCollectionUri(
    databaseId,
    collectionId
  );
  client
    .queryDocuments(
      collectionUrl,
      `SELECT ${countConstraint} * FROM c where c.Type= "${req.params.type}"`
    )
    .toArray((error, results) => {
      checkError(res, error, results);
    });
}

function updateDocument(req, res) {
  //TODO: get id from params, for some reason params is undefined
  //console.log(req.req);
  const id = req.params.id;
  let documentUrl = uriFactory.createDocumentUri(databaseId, collectionId, id);

  client.replaceDocument(documentUrl, req.body, (error, result) => {
    checkError(res, error, result);
  });
}

function deleteDocument(req, res) {
  const id = req.params.id;
  // console.log(`Deleting document:\n${document.id}\n`);
  let documentUrl = uriFactory.createDocumentUri(databaseId, collectionId, id);
  client.deleteDocument(documentUrl, (err, result) => {
    checkError(res, err, result);
  });
}

module.exports = {
  queryCollection,
  updateDocument,
  createDocument,
  deleteDocument
};
