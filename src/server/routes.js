const express = require("express");

const router = express.Router();

const docDbConnector = require("./doc-db-connector.service");

const util = require("util");

const clipboardy = require("clipboardy");

router.get("/queryCollection/:type", (req, res) => {
  docDbConnector.queryCollection(req, res);
});

router.put("/updateDocument/:id", (req, res) => {
  docDbConnector.updateDocument(req, res);
});

router.post("/createDocument", (req, res) => {
  docDbConnector.createDocument(req, res);
});

router.delete("/deleteDocument/:id", (req, res)=>{
  docDbConnector.deleteDocument(req, res);
});

module.exports = router;
